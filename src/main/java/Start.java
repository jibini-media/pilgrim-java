import net.jibini.pilgrim.Pilgrim;

public class Start
{
	public static void main(String[] args)
	{
		Pilgrim pilgrim = new Pilgrim();
		Pilgrim.setPilgrim(pilgrim);
		pilgrim.start();
		System.exit(0);
	}
}
