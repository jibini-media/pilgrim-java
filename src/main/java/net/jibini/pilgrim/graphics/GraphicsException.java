package net.jibini.pilgrim.graphics;

public class GraphicsException extends RuntimeException
{
	private static final long serialVersionUID = 1L;

	public GraphicsException(String desc, Throwable cause)
	{
		super(desc, cause);
	}

	public GraphicsException(String desc)
	{
		super(desc);
	}
}
