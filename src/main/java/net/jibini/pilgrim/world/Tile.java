package net.jibini.pilgrim.world;

import net.jibini.pilgrim.world.material.Material;

public class Tile
{
	private final long x, y;
	private Material material;
	
	public Tile(long x, long y, Material material)
	{
		this.x = x;
		this.y = y;
		this.material = material;
	}
	
	public long getX()
	{
		return x;
	}
	
	public long getY()
	{
		return y;
	}
	
	public Material getMaterial()
	{
		return material;
	}
	
	public void setMaterial(Material material)
	{
		this.material = material;
	}
}
