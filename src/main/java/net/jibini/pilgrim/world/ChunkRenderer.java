package net.jibini.pilgrim.world;

import org.lwjgl.opengl.GL11;

public class ChunkRenderer
{
	public static final float TILE_SIZE = 1.0f;

	private int displayList = -1;
	private Chunk chunk;
	
	public ChunkRenderer(Chunk chunk)
	{
		this.chunk = chunk;
	}
	
	public void build()
	{
		if (displayList != -1)
			GL11.glDeleteLists(displayList, 1);
		displayList = GL11.glGenLists(1);
		
		GL11.glNewList(displayList, GL11.GL_COMPILE);
		for (int b = 0; b < Chunk.CHUNK_WIDTH; b ++)
			for (int a = 0; a < Chunk.CHUNK_HEIGHT; a ++)
				renderTile(chunk.getTiles()[a][b]);
		GL11.glEndList();
	}
	
	private void renderTile(Tile tile)
	{
		GL11.glPushAttrib(GL11.GL_COLOR_BUFFER_BIT);
		tile.getMaterial().getTint().setColorAttrib();
		
		GL11.glBegin(GL11.GL_QUADS);
		GL11.glVertex2f(tile.getX() - TILE_SIZE / 2, tile.getY() + TILE_SIZE / 2);
		GL11.glVertex2f(tile.getX() + TILE_SIZE / 2, tile.getY() + TILE_SIZE / 2);
		GL11.glVertex2f(tile.getX() + TILE_SIZE / 2, tile.getY() - TILE_SIZE / 2);
		GL11.glVertex2f(tile.getX() - TILE_SIZE / 2, tile.getY() - TILE_SIZE / 2);
		GL11.glEnd();
		GL11.glPopAttrib();
	}
	
	public void render()
	{
		GL11.glCallList(displayList);
	}
	
	public int getDisplayList()
	{
		return displayList;
	}
}
