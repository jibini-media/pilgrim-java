package net.jibini.pilgrim.world.material;

import java.io.Serializable;

import net.jibini.pilgrim.color.Color;

public interface Material extends Serializable
{
	Color getTint();
	
	long getID();
	
	String getName();
	
	boolean equals(Material material);
}
