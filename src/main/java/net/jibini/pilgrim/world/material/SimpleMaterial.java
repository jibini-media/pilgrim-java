package net.jibini.pilgrim.world.material;

import net.jibini.pilgrim.color.Color;

public class SimpleMaterial implements Material
{
	private final Color color;
	private final long id;
	private final String name;
	
	public SimpleMaterial(Color color, long id, String name)
	{
		this.color = color;
		this.id = id;
		this.name = name;
	}
	
	@Override
	public Color getTint()
	{
		return color;
	}

	@Override
	public long getID()
	{
		return id;
	}

	@Override
	public String getName()
	{
		return name;
	}
	
	@Override
	public boolean equals(Material material)
	{
		return material.getID() == getID();
	}
}
