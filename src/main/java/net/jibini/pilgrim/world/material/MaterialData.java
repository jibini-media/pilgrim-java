package net.jibini.pilgrim.world.material;

public class MaterialData
{
	private Material material;
	
	public MaterialData(Material material)
	{
		this.material = material;
	}
	
	public Material getMaterial()
	{
		return material;
	}
}
