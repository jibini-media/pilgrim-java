package net.jibini.pilgrim.world.material;

import net.jibini.pilgrim.ObjectRegistry;

public class MaterialDataRegistry extends ObjectRegistry<Long, MaterialData>
{
	public void registerMaterial(Material material)
	{
		put(material.getID(), new MaterialData(material));
	}
	
	public MaterialData getByName(String name)
	{
		for (MaterialData material : this.values())
			if (material.getMaterial().getName().equals(name))
				return material;
		return null;
	}
	
	public MaterialData getByMaterial(Material material)
	{
		for (MaterialData materialData : this.values())
			if (materialData.getMaterial().equals(material))
				return materialData;
		return null;
	}
}
