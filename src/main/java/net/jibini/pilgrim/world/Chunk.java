package net.jibini.pilgrim.world;

import java.io.Serializable;
import java.util.Random;

import net.jibini.pilgrim.Pilgrim;
import net.jibini.pilgrim.world.material.Material;
import net.jibini.pilgrim.world.material.MaterialDataRegistry;

public class Chunk implements Serializable
{
	public static final int CHUNK_WIDTH = 32;
	public static final int CHUNK_HEIGHT = 32;
	
	private final long x, y;
	
	private Tile[][] tiles = new Tile[CHUNK_WIDTH][CHUNK_HEIGHT];
	
	public Chunk(long x, long y, boolean inflate)
	{
		this.x = x;
		this.y = y;
		if (inflate)
			inflate();
	}
	
	public Chunk(long x, long y)
	{
		this(x, y, true);
	}
	
	public void inflate()
	{
		Random rand = new Random();
		rand.setSeed(rand.nextLong());
		MaterialDataRegistry materialRegistry = Pilgrim.getPilgrim().getMaterialRegistry();
		
		for (int b = 0; b < CHUNK_WIDTH; b ++)
			for (int a = 0; a < CHUNK_HEIGHT; a ++)
			{
				Material randMaterial = materialRegistry.getByKey((long) rand.nextInt(materialRegistry.size())).getMaterial();
				Tile tile = new Tile(x * CHUNK_WIDTH + a, y * CHUNK_HEIGHT + b, randMaterial);
				tiles[a][b] = tile;
			}
	}
	
	public long getX()
	{
		return x;
	}
	
	public long getY()
	{
		return y;
	}
	
	public Tile[][] getTiles()
	{
		return tiles;
	}
}
