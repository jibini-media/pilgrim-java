package net.jibini.pilgrim;

import java.util.HashMap;

public class ObjectRegistry<K, V> extends HashMap<K, V>
{
	public V getByKey(K key)
	{
		return get((K) key);
	}
}
