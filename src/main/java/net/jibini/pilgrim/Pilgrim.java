package net.jibini.pilgrim;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.opengl.GL11;

import net.jibini.pilgrim.color.Colors;
import net.jibini.pilgrim.world.Chunk;
import net.jibini.pilgrim.world.ChunkRenderer;
import net.jibini.pilgrim.world.material.MaterialDataRegistry;
import net.jibini.pilgrim.world.material.SimpleMaterial;

public class Pilgrim extends Game
{
	public Pilgrim()
	{
		super("Pilgrim pre-alpha");
	}
	
	private ChunkRenderer testRenderer;
	private Chunk testChunk;
	private MaterialDataRegistry materialRegistry = new MaterialDataRegistry();

	@Override
	public void handleKey(long window, int key, int scancode, int action, int mods)
	{
		if (action == GLFW.GLFW_RELEASE)
			if (key == GLFW.GLFW_KEY_R)
			{
				testChunk.inflate();
				testRenderer.build();
			}
	}

	@Override
	public void init()
	{
		initGL();
		registerMaterials();
		
		testChunk = new Chunk(0L, 0L);
		testRenderer = new ChunkRenderer(testChunk);
		testRenderer.build();
	}
	
	private void initGL()
	{
		GLFW.glfwSwapInterval(0);
		GL11.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
		
		GL11.glMatrixMode(GL11.GL_PROJECTION_MATRIX);
		GL11.glOrtho(0, 800 / 5, 0, 450 / 5, -100, 100);
		GL11.glMatrixMode(GL11.GL_MODELVIEW_MATRIX);
	}
	
	private void registerMaterials()
	{
		materialRegistry.registerMaterial(new SimpleMaterial(Colors.COLOR_CLEAR, 0L, "material.void"));
		materialRegistry.registerMaterial(new SimpleMaterial(Colors.COLOR_GREEN, 1L, "material.grass"));
		materialRegistry.registerMaterial(new SimpleMaterial(Colors.COLOR_BLUE, 2L, "material.water"));
	}

	@Override
	public void render()
	{
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
		
		testRenderer.render();
	}

	@Override
	public void destroy()
	{
		
	}
	
	public MaterialDataRegistry getMaterialRegistry()
	{
		return materialRegistry;
	}
	
	private static Pilgrim pilgrim;
	
	public static Pilgrim getPilgrim()
	{
		return pilgrim;
	}
	
	public static void setPilgrim(Pilgrim pilgrim)
	{
		Pilgrim.pilgrim = pilgrim;
	}
}
