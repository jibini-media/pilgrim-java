package net.jibini.pilgrim;

import org.lwjgl.Version;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.opengl.GL;

import net.jibini.pilgrim.graphics.GraphicsException;
import net.jibini.pilgrim.graphics.SharedLibraryLoader;

public abstract class Game
{
	private final String title;
	private long window;
	
	public Game(String title)
	{
		this.title = title;
	}
	
	public void start()
	{
		initContext();
		loop();
		destroyContext();
	}
	
	private void initContext()
	{
		System.out.println("LWJGL Build: " + Version.getVersion());
		GLFWErrorCallback.createPrint(System.err).set();
		
		String failed = "Failed to init GLFW - perhaps update graphics drivers";
		SharedLibraryLoader.load();
		if (!GLFW.glfwInit())
			throw new GraphicsException(failed);
		
		GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_VERSION_MAJOR, 2);
		GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_VERSION_MINOR, 0);
		GLFW.glfwWindowHint(GLFW.GLFW_RESIZABLE, GLFW.GLFW_FALSE);
		GLFW.glfwWindowHint(GLFW.GLFW_VISIBLE, GLFW.GLFW_FALSE);
		
		window = GLFW.glfwCreateWindow(800, 450, title, 0L, 0L);
		if (window == 0L)
			throw new GraphicsException(failed);
		GLFW.glfwSetKeyCallback(window, this::handleKey);
		
		GLFW.glfwMakeContextCurrent(window);
		GL.createCapabilities();
		GLFW.glfwShowWindow(window);
	}
	
	private void destroyContext()
	{
		GLFW.glfwDestroyWindow(window);
		GLFW.glfwTerminate();
	}
	
	public abstract void handleKey(long window, int key, int scancode, int action, int mods);
	
	public void loop()
	{
		init();
		
		while (!GLFW.glfwWindowShouldClose(window))
		{
			render();
			
			GLFW.glfwSwapBuffers(window);
			GLFW.glfwPollEvents();
		}
		
		destroy();
	}
	
	public long getWindow()
	{
		return window;
	}
	
	public abstract void init();
	
	public abstract void render();
	
	public abstract void destroy();
}
