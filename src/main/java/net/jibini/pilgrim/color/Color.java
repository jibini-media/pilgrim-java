package net.jibini.pilgrim.color;

import org.lwjgl.opengl.GL11;

public class Color
{
	private float r, g, b, a;
	
	public Color(float r, float g, float b, float a)
	{
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}
	
	public Color(float r, float g, float b)
	{
		this(r, g, b, 1.0f);
	}
	
	public void setColorAttrib()
	{
		GL11.glColor4f(r, g, b, a);
	}
	
	public float getRed()
	{
		return r;
	}
	
	public float getGreen()
	{
		return g;
	}
	
	public float getBlue()
	{
		return b;
	}
	
	public float getAlpha()
	{
		return a;
	}
	
	public void setRed(float r)
	{
		this.r = r;
	}
	
	public void setGreen(float g)
	{
		this.g = g;
	}
	
	public void setBlue(float b)
	{
		this.b = b;
	}
	
	public void setAlpha(float a)
	{
		this.a = a;
	}
}
