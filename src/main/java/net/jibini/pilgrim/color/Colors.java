package net.jibini.pilgrim.color;

public class Colors
{
	public static final Color COLOR_RED = new Color(1.0f, 0.0f, 0.0f);
	public static final Color COLOR_GREEN = new Color(0.0f, 1.0f, 0.0f);
	public static final Color COLOR_BLUE = new Color(0.0f, 0.0f, 1.0f);
	
	public static final Color COLOR_BLACK = new Color(0.0f, 0.0f, 0.0f);
	public static final Color COLOR_WHITE = new Color(1.0f, 0.0f, 0.0f);
	public static final Color COLOR_CLEAR = new Color(0.0f, 0.0f, 0.0f, 0.0f);
}
